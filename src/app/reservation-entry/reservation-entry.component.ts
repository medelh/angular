import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reservation-entry',
  templateUrl: './reservation-entry.component.html',
  styleUrls: ['./reservation-entry.component.css']
})
export class ReservationEntryComponent implements OnInit {

  //Properties
  propName: String;

  constructor() { }

  ngOnInit() {
    this.propName = "David";
  }

  reservationEntry(): void{
    console.log(this.propName);
  }
}
